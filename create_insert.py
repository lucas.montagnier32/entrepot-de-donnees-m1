
import os
import csv
import numpy as np

id_commune=1

def str_to_int(cdc):
	if cdc=="O":
		return 1
	else:
		return 0

with open("./liste-des-gares.csv") as csvfile, open("insert_gare.sql", "w") as result_gare, open("insert_commune.sql","w") as result_commune:
    reader = csv.reader(csvfile, delimiter=';')
    row = next(reader) # les entetes de colonnes
    print("--- create insert Gare and Commune")
    for row in reader:
        nom_gare = str(row[1].replace("é","e").replace("î","i").upper().replace("'"," ").replace("-"," "))
        if nom_gare=="PARIS GARE DE LYON": nom_gare="PARIS LYON"
        result_gare.write("insert into Gare values ('"+nom_gare+"',"+str(id_commune)+","+str(str_to_int(row[2]))+","+str(str_to_int(row[3]))+");\n")
        result_commune.write("insert into Commune values ("+str(id_commune)+",'"+str(row[7].replace("'"," "))+"','"+str(row[8].replace("'"," "))+"');\n")
        id_commune += 1
    result_gare.write("insert into Gare values ('PARIS EST', 208, 1, 1);\n")



id_dates=1
old_year=0
old_month=0

id_retard=1

with open("./Regularities_by_liaisons_Trains_France.csv") as csvfile, open("insert_dates.sql", "w") as result_date, open("insert_retard.sql", "w") as result_retard, open("insert_trajet.sql", "w") as result_trajet:
    reader = csv.reader(csvfile, delimiter=',')
    row = next(reader) # les entetes de colonnes
    print("--- create insert Dates, Retard and Trajet")
    for row in reader:
        result_trajet.write("insert into Trajet values ('"+row[2]+"','"+row[3]+"',"+str(id_dates)+","+str(id_retard)+","+row[4]+","+row[5]+","+row[6]+","+row[7]+");\n")
        if old_year!=int(row[0]) and old_month!=int(float(row[1])):
            old_year = int(row[0])
            old_month = int(float(row[1]))
            result_date.write("insert into Dates values ("+str(id_dates)+","+str(old_year)+","+str(old_month)+");\n")
            id_dates += 1
        result_retard.write("insert into Retard values ("+str(id_retard)+","+row[26]+","+row[27]+","+row[28]+","+row[29]+","+row[30]+","+row[31]+");\n")
        id_retard += 1

        
