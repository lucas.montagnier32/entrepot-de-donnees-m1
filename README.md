
### Description des datasets:

#### Dataset des trajets: (tables Trajet, Dates et Retard)
Ce dataset présente des données sur les retards des trains SNCF sur différents trajets de l'année 2015 à 2020. On y trouve différentes causes de retard (problème matériel, problème d'organisation, problème de voyageurs, etc.) ainsi que les données numériques associées, comme les nombres de retards et les pourcentages de retard selon les différentes causes par mois et par année.

https://www.kaggle.com/datasets/gatandubuc/public-transport-traffic-data-in-france

#### Dataset des gares: (tables Gare et Commune)
Ce dataset nous a permis d'associer les gares présentes dans le premier dataset à leur commune. Il donne aussi différentes précisions sur les gares, comme le fait que ce soit des gares de frets (transports de marchandises et matières premières), des gares voyageurs, ou les deux.
https://www.data.gouv.fr/fr/datasets/liste-des-gares/

Les deux datasets sont fournis par la SNCF via le programme OPEN DATA et sont distribués sous licence Open Database License (ODbL).

--------------------------------------------------------------
### Execution
Le fichier create_insert.py permet de génerer tous les fichiers d'insertions à partir des deux fichiers csv présents qui doivent être dans le même dossier que le script. La commande d'exécution est la suivante: `python3 create_insert.py`. Ensuite, il suffit d'exécuter le fichier create_tables.sql puis tous les fichier insert dans la base de données.

--------------------------------------------------------------
### Contributeurs:
- Malo Le Reste
- Bilal Molli
- Lucas Montagnier
- Tristan Ramé
