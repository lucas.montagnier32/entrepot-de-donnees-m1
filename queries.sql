-- Q1
-- top 20 des trajet ayant le plus d'annulations'
select s.* from (
	select t.id_station_dep, t.id_station_arr, sum(t.nb_cancelled_trains) as total_cancelled_trains
	from Trajet t
	group by t.id_station_dep, t.id_station_arr
	order by total_cancelled_trains desc) s
--limit 20;
where rownum<=20;


-- Q2
-- nombre moyen de trains par trajet par departement et au total
select c.departement, t.id_station_dep, round(avg(t.nb_trains), 2) as avg_trains
from Commune c, Gare g, Trajet t
where c.id_commune=g.id_commune AND g.nom=t.id_station_dep 
group by rollup(c.departement, t.id_station_dep)
order by c.departement, t.id_station_dep;


-- Q3
-- nombre de trains annulés par mois et au total
select d.mois, sum(t.nb_cancelled_trains) as nb_cancelled_trains, sum(t.nb_trains) as nb_trains,
	round(sum(t.nb_cancelled_trains)*100.0/sum(t.nb_trains), 2) as percent_cancelled_trains
from Dates d, Trajet t
where d.id_date=t.id_date
group by rollup(d.mois)
order by d.mois;


-- nb de trains en retard par cause de retard (sans doute à utiliser pour le rapport/diapo mais pas pour les 10 requêtes)
select sum(r.due_to_external_causes) as external_causes, sum(r.due_to_railway_infrastructure) as railway_infrastructure,
	sum(r.due_to_traffic_management) as traffic_management, sum(due_to_rolling_stock) as rolling_stock,
	sum(r.due_to_station_management) as station_management, sum(r.due_to_travellers_taken) as travellers_taken,
	sum(t.nb_late_trains)
from trajet t, retard r
where t.id_retard = r.id_retard;


-- Q4
-- trajets les plus longs en temps et leur stabilité
select t.id_station_dep, t.id_station_arr, round(avg(t.average_travel_time), 2) as travel_time,
--	sum(nb_trains) as trains, sum(nb_late_trains) as late_trains, sum(nb_cancelled_trains) as cancelled_trains
	round(sum(t.nb_late_trains)*100.0/sum(t.nb_trains), 2) as percent_late,
	round(sum(t.nb_cancelled_trains)*100.0/sum(t.nb_trains), 2) as percent_cancelled
from trajet t
group by t.id_station_dep, t.id_station_arr
order by avg(t.average_travel_time) desc
--limit 20
--where rownum<=20
;


-- Q5
-- Evolution du nombre de trains par trajet au court des années
select t.id_station_dep, t.id_station_arr, d.annee, sum(t.nb_trains) as nb_trains
from trajet t, dates d
where t.id_date = d.id_date
group by grouping sets((t.id_station_dep, t.id_station_arr, d.annee), (t.id_station_dep, t.id_station_arr), ())
order by t.id_station_dep, t.id_station_arr, d.annee;


-- Q6
-- Pourcentage de retard dus à la gestion du traffique par trajet (dans l'objectif de trouver les trajets qui coincent)
select t.id_station_dep, t.id_station_arr,
	round(avg(r.due_to_traffic_management), 2) as percent_traffic_management,
	round(sum(t.nb_late_trains)*100.0/sum(t.nb_trains), 2) as percent_late_trains
from trajet t, retard r
where t.id_retard = r.id_retard
group by t.id_station_dep, t.id_station_arr
order by percent_traffic_management desc;


-- Q7
-- Pourcentage de perturbations (trains en retard et annulés) par trajets (cube)
select t.id_station_dep, t.id_station_arr,
	round((sum(t.nb_late_trains)+sum(t.nb_cancelled_trains))*100.0/sum(t.nb_trains), 2) as percent_perturbations
from trajet t, dates d
where t.id_date = d.id_date
group by cube(t.id_station_dep, t.id_station_arr)
order by t.id_station_dep, t.id_station_arr;


-- Q8
-- Pourcentage de perturbations par trajets partant de Paris Montparnasse et par années (cube)
select t.id_station_arr, d.annee,
	round((sum(t.nb_late_trains)+sum(t.nb_cancelled_trains))*100.0/sum(t.nb_trains), 2) as percent_perturbations
from trajet t, dates d
where t.id_date = d.id_date and t.id_station_dep like 'PARIS MONTPARNASSE'
group by cube(t.id_station_arr, d.annee)
order by t.id_station_arr, d.annee;


-- Q9
-- Pourcentage de perturbations par années et par mois (cube)
select d.annee, d.mois,
	round((sum(t.nb_late_trains)+sum(t.nb_cancelled_trains))*100.0/sum(t.nb_trains), 2) as percent_perturbations
from trajet t, dates d
where t.id_date = d.id_date
group by cube(d.annee, d.mois)
order by d.annee, d.mois;


-- Q10
-- Moyenne glissante du pourcentage de perturbation par gare de départ et par mois
select s.id_station_dep, s.mois, s.total_trains,
	round((s.nb_late_trains+s.nb_cancelled_trains)*100.0/s.nb_trains, 2) as percent_perturbations
from (
	select t.id_station_dep, d.mois, sum(t.nb_trains) as total_trains,
        sum(sum(t.nb_trains)) over (partition by t.id_station_dep order by d.mois range between 1 preceding and 1 following) as nb_trains,
        sum(sum(t.nb_late_trains)) over (partition by t.id_station_dep order by d.mois range between 1 preceding and 1 following) as nb_late_trains,
        sum(sum(t.nb_cancelled_trains)) over (partition by t.id_station_dep order by d.mois range between 1 preceding and 1 following) as nb_cancelled_trains
		--sum(sum(t.nb_trains)) over range_window as nb_trains,
		--sum(sum(t.nb_late_trains)) over range_window as nb_late_trains,
		--sum(sum(t.nb_cancelled_trains)) over range_window as nb_cancelled_trains
	from trajet t, dates d
	where t.id_date = d.id_date
		and d.annee != 2020
		and (d.annee != 2019 or d.mois != 12)
	group by grouping sets((t.id_station_dep, d.mois), (t.id_station_dep), ())
	--window range_window as (partition by t.id_station_dep order by d.mois range between 1 preceding and 1 following)
) s
order by s.id_station_dep, s.mois
;


