set linesize 200;

prompt *************************************************************
prompt ******************** DROP TABLE *****************************
prompt *************************************************************

drop table Commune cascade constraints;
drop table Gare cascade constraints;
drop table Dates cascade constraints;
drop table Retard cascade constraints;
drop table Trajet cascade constraints;

prompt *************************************************************
prompt ******************** CREATE TABLE ***************************
prompt *************************************************************

create table Commune (
    id_commune number,
    nom varchar2(50),
    departement varchar2(50),
    constraint pk_commune primary key (id_commune)
);

create table Gare (
    nom varchar2(50),
    id_commune number,
    fret number, -- boolean
    voyageurs number, -- boolean
    constraint pk_gare primary key (nom),
    constraint fk_gare_commune foreign key (id_commune) references Commune(id_commune)
);

create table Dates (
    id_date number,
    annee number,
    mois number,
    constraint pk_dates primary key (id_date)
);

create table Retard (
    id_retard number,
    due_to_external_causes number,
    due_to_railway_infrastructure number,
    due_to_traffic_management number,
    due_to_rolling_stock number,
    due_to_station_management number,
    due_to_travellers_taken number,
    constraint pk_retard primary key (id_retard)
);

create table Trajet (
    id_station_dep varchar2(50),
    id_station_arr varchar2(50),
    id_date number,
    id_retard number,
    average_travel_time number,
    nb_trains number,
    nb_cancelled_trains number,
    nb_late_trains number,
    constraint pk_trajet primary key (id_station_dep, id_station_arr, id_date, id_retard),
    constraint fk_trajet_dates foreign key (id_date) references Dates(id_date),
    constraint fk_trajet_retard foreign key (id_retard) references Retard(id_retard),
    constraint fk_trajet_gare_dep foreign key (id_station_dep) references Gare(nom),
    constraint fk_trajet_gare_arr foreign key (id_station_arr) references Gare(nom)
);

prompt *************************************************************
prompt ********************* DROP ROLE *****************************
prompt *************************************************************

DROP ROLE M1_MBLT_client;
DROP ROLE M1_MBLT_admin;
DROP ROLE M1_MBLT_voyageur;

prompt *************************************************************
prompt ******************** CREATE ROLE ****************************
prompt *************************************************************
--ADMI24=MALO, ADMI29=BILAL, ADMI30=LUCAS, ADMI34=TRISTAN

CREATE ROLE M1_MBLT_client;
CREATE ROLE M1_MBLT_admin;
CREATE ROLE M1_MBLT_voyageur;

GRANT M1_MBLT_admin TO ADMI24;
GRANT M1_MBLT_voyageur TO ADMI30;
GRANT M1_MBLT_client TO ADMI29;
GRANT M1_MBLT_voyageur TO ADMI34;

GRANT ALL ON Commune TO M1_MBLT_admin;
GRANT ALL ON Gare TO M1_MBLT_admin;
GRANT ALL ON Dates TO M1_MBLT_admin;
GRANT ALL ON Retard TO M1_MBLT_admin;
GRANT ALL ON Trajet TO M1_MBLT_admin;

GRANT select on Commune TO M1_MBLT_client;
GRANT select on Gare TO M1_MBLT_client;
GRANT select on Dates TO M1_MBLT_client;
GRANT select on Retard TO M1_MBLT_client;
GRANT select on Trajet TO M1_MBLT_client;

GRANT select on Commune TO M1_MBLT_voyageur;
GRANT select on Gare TO M1_MBLT_voyageur;
GRANT select on Dates TO M1_MBLT_voyageur;
GRANT select on Retard TO M1_MBLT_voyageur;
GRANT select on Trajet TO M1_MBLT_voyageur;

prompt *************************************************************
prompt ********************* CREATE VPD ****************************
prompt *************************************************************

-- les voyageurs ne pourront voir que les gares qui prennent des voyageurs et pas celles qui prennent du fret
CREATE OR REPLACE FUNCTION auth_voyageur(
		schema_var IN VARCHAR2,
		table_var IN VARCHAR2
	)
	RETURN VARCHAR2
	IS
		return_val VARCHAR2 (400);
BEGIN
	return_val := 'voyageurs = 1';
	RETURN return_val;
END auth_voyageur;
/
show errors;

BEGIN
	DBMS_RLS.ADD_POLICY (
		OBJECT_SCHEMA => 'admi30',
		OBJECT_NAME => 'Gare',
		POLICY_NAME => 'CLIENTS_POLICY',
		FUNCTION_SCHEMA => 'admi30',
		POLICY_FUNCTION => 'AUTH_VOYAGEUR',
		STATEMENT_TYPES => 'SELECT'
	);
END;
/
show errors;

--COMMIT;
